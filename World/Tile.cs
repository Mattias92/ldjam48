﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam48.World
{
    public class Tile
    {
        public int U { get; set; }
        public int V { get; set; }
        public bool IsSolid { get; set; }
        public bool CanBeDug { get; set; }
        public bool IsBomb { get; set; }
        public int NumberOfNearbyBombs { get; set; }
        public bool IsExplored { get; set; }
        public double Score { get; set; }
        public bool IsLadder { get; set; }

        public Tile()
        {
            IsSolid = true;
            IsBomb = false;
            NumberOfNearbyBombs = 0;
            Score = 0;
            IsExplored = false;
            IsLadder = false;
        }
    }
}

﻿using LDJam48.Entities;
using LDJam48.World.Tiles;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace LDJam48.World
{
    public class Gameboard
    {
        public readonly ContentManager ContentManager;
        public readonly Camera Camera;
        public double Score { get; set; }
        public int Lives { get; set; }

        private int screenWidth;

        private Texture2D texture;
        private SpriteFont testFont;
        public static int Width = 25;
        public static int Height = 1024;
        private Tile[,] tiles;
        public List<Entity> Entities;
        public int idCounter = 0;

        public Tile this[int x, int y]
        {
            get
            {
                if (x >= 0 && x < Width && y >= 0 && y < Height)
                {
                    return tiles[x, y];
                }
                return null;
            }
            set
            {
                tiles[x, y] = value;
            }
        }
        public Tile this[Point point]
        {
            get
            {
                return this[point.X, point.Y];
            }
            set
            {
                this[point.X, point.Y] = value;
            }
        }

        public Gameboard(ContentManager contentManager, Camera camera, int screenWidth)
        {
            ContentManager = contentManager;
            Camera = camera;
            texture = contentManager.Load<Texture2D>("tiles/atlas9");
            testFont = contentManager.Load<SpriteFont>("LatoRegular12");
            Entities = new List<Entity>();
            Entities.Add(new Player(idCounter++, this, new Vector2(12 * 32, 7 * 32)));
            GenerateGameboard();
            Score = 0;
            Lives = 3;
            this.screenWidth = screenWidth; 
        }

        private void GenerateGameboard()
        {
            tiles = new Tile[Width, Height];
            Random random = new Random();

            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    if (x == 0)
                    {
                        tiles[x, y] = new BorderTile();
                    }
                    else if (x == Width - 1)
                    {
                        tiles[x, y] = new BorderTile();
                    }
                    else if (y >= 8)
                    {
                        Tile tile;

                        if (y == 8)
                        {
                            tile = new TopDirtTile();
                            tile.IsExplored = true;
                        }
                        else if (y > 60)
                        {
                            tile = new RockTile();
                        }
                        else if (y > 40)
                        {
                            double tileChance = random.NextDouble();

                            if (tileChance > 0.5)
                            {
                                tile = new RockTile();
                            }
                            else
                            {
                                tile = new DirtTile();
                            }
                        }
                        else
                        {
                            tile = new DirtTile();
                        }

                        int chanceOfGold = random.Next(0, 100);

                        if (chanceOfGold > 90 && y != 8)
                        {
                            chanceOfGold = random.Next(0, 100);

                            if (chanceOfGold > 95)
                            {
                                tile.Score = 500;
                            }
                            else
                            {
                                tile.Score = 100;
                            }
                        }

                        double chanceOfBomb = random.NextDouble();

                        if (chanceOfBomb > 0.9 - (y - 20) * 0.00001 * (y / 5))
                        {
                            tile.IsBomb = true;
                        }

                        tiles[x, y] = tile;
                    }

                    double chanceOfChest = random.NextDouble();

                    if (y >= 9 && x != 0 && x != Width - 1 && chanceOfChest > 0.995)
                    {
                        tiles[x, y] = new DugOutTile(0);

                        chanceOfChest = random.NextDouble();

                        if (chanceOfChest > 0.8)
                        {
                            Entities.Add(new Chest(idCounter++, this, new Vector2(x * 32, y * 32), Chest.RewardType.Life));
                        }
                        else
                        {
                            Entities.Add(new Chest(idCounter++, this, new Vector2(x * 32, y * 32), Chest.RewardType.Currency));
                        }
                    }
                }
            }

            CalculateBombNumbers();
        }

        public void CalculateBombNumbers()
        {
            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    Tile tile = tiles[x, y];
  
                    if (tile != null && !tile.IsBomb && !(x == 0 || x == Width - 1))
                    {
                        tile.NumberOfNearbyBombs = 0;
                    }
                    else
                    {
                        continue;
                    }

                    for (int nx = -1; nx <= 1; nx++)
                    {
                        for (int ny = -1; ny <= 1; ny++)
                        {
                            if (nx == 0 && ny == 0)
                            {
                                continue;
                            }

                            Tile nearbyTile = this[x + nx, y + ny];

                            if (nearbyTile != null && nearbyTile.IsBomb && tile != null)
                            {
                                tile.NumberOfNearbyBombs++;
                            }
                        }
                    }
                }
            }
        }

        public void Update(float delta)
        {
            for (int i = 0; i < Entities.Count; i++)
            {
                Entities[i].Update(delta);
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    if (tiles[x, y] != null)
                    {
                        Tile tile = tiles[x, y];
                        spriteBatch.Draw(texture, new Vector2(x * 32, y *32), new Rectangle(tile.U * 32, tile.V * 32, 32, 32), Color.White);

                        if (tile.Score == 100)
                        {
                            spriteBatch.Draw(texture, new Vector2(x * 32, y * 32), new Rectangle(4 * 32, 1* 32, 32, 32), Color.White);
                        }
                        else if (tile.Score > 100)
                        {
                            spriteBatch.Draw(texture, new Vector2(x * 32, y * 32), new Rectangle(3 * 32, 1 * 32, 32, 32), Color.White);
                        }

                        if (tile.IsLadder)
                        {
                            spriteBatch.Draw(texture, new Vector2(x * 32, y * 32), new Rectangle(2 * 32, 0 * 32, 32, 32), Color.White);
                        }

                        string bombStr = tile.NumberOfNearbyBombs.ToString();
                        Vector2 bombStrSize = testFont.MeasureString(bombStr);

                        if (tile.IsExplored && tile.NumberOfNearbyBombs > 0)
                        {
                            spriteBatch.DrawString(testFont, tile.NumberOfNearbyBombs.ToString(), new Vector2(x * 32 + 16, y * 32 + 16) - bombStrSize / 2, Color.White);
                        }
                    }
                }
            }

            for (int i = 0; i < Entities.Count; i++)
            {
                Entities[i].Draw(spriteBatch);
            }

            string scoreStr = "$" + Score.ToString();
            Vector2 scoreStrSize = testFont.MeasureString(scoreStr);
            spriteBatch.DrawString(testFont, scoreStr, new Vector2(screenWidth - scoreStrSize.X - 8f, -Camera.Position.Y), Color.White);
        }

        public void DrawLives(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < Lives; i++)
            {
                spriteBatch.Draw(texture, new Vector2(0 + i * 32, 0), new Rectangle(1 * 32, 2 * 32, 32, 32), Color.White);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam48.World.Tiles
{
    public class RockTile : Tile
    {
        public RockTile()
        {
            U = 0;
            V = 1;
            CanBeDug = true;
        }
    }
}

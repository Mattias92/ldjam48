﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam48.World.Tiles
{
    public class TopDirtTile : DirtTile
    {
        public TopDirtTile()
        {
            U = 0;
            V = 2;
        }
    }
}

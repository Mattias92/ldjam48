﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam48.World.Tiles
{
    public class BorderTile : Tile
    {
        public BorderTile()
        {
            U = 1;
            V = 0;
            CanBeDug = false;
        }
    }
}

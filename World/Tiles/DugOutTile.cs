﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam48.World.Tiles
{
    public class DugOutTile : Tile
    {
        public DugOutTile(int numberOfNearbyBombs)
        {
            U = 2;
            V = 1;
            IsSolid = false;
            NumberOfNearbyBombs = numberOfNearbyBombs;
            IsExplored = true;
        }
    }
}

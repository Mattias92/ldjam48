﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam48.World.Tiles
{
    public class DirtTile : Tile
    {
        public DirtTile()
        {
            U = 0;
            V = 0;
            CanBeDug = true;
        }
    }
}

﻿using LDJam48.World;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam48.Entities
{
    public class Chest : Entity
    {
        public enum RewardType
        {
            Life,
            Currency
        }

        public RewardType Reward { get; set; }

        public Chest(int id, Gameboard gameboard, Vector2 position, RewardType reward) : base(id, gameboard)
        {
            Reward = reward;
            U = 2;
            V = 2;
            texturePath = "tiles/atlas9";
            texture = gameboard.ContentManager.Load<Texture2D>(texturePath);
            Position = position;
        }

        public void ProcessReward()
        {
            switch (Reward)
            {
                case RewardType.Currency:
                    gameboard.Score += 1000;
                    break;
                case RewardType.Life:
                    gameboard.Lives += 1;
                    break;
            }
        }

        public override void Update(float delta)
        {
            Point TilePositionPointBelow = new Point((int)Center.X / 32, (int)Center.Y / 32 + (int)-Vector3.Down.Y);
            bool isFalling = false;

            if (gameboard[TilePositionPointBelow] == null || !gameboard[TilePositionPointBelow].IsSolid)
            {
                isFalling = true;
            }
            else if (TilePositionPointBelow.Y * 32 > Bottom)
            {
                isFalling = true;
            }

            if (isFalling)
            {
                float distance = TilePositionPointBelow.Y * 32 - Bottom;

                if (distance > 3f)
                {
                    Y = CenterTilePosition.Y * 32;
                }
                else
                {
                    Y += 3f;
                }
            }
        }
    }
}

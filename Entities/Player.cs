﻿using LDJam48.World;
using LDJam48.World.Tiles;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam48.Entities
{
    public class Player : Entity
    {
        private KeyboardState previousKeyboardState = new KeyboardState();
        private KeyboardState keyboardState;
        private int diggingTimer = 0;
        private Vector3 _lastDirection;
        private Vector3 lastDirection
        {
            get
            {
                return _lastDirection;
            }
            set
            {
                if (_lastDirection != value)
                {
                    diggingTimer = 0;
                    animationTimer = 0;
                }
                _lastDirection = value;
            }
        }
        private float amountToFall = 0;
        private int animationTimer = 0;

        public Player(int id, Gameboard gameboard, Vector2 position) : base(id, gameboard)
        {
            texturePath = "tiles/atlas9";
            texture = gameboard.ContentManager.Load<Texture2D>(texturePath);
            Position = position;
            U = 0;
            V = 3;
        }

        public override void Update(float delta)
        {
            keyboardState = Keyboard.GetState();
            Point centerTile = CenterTilePosition;

            Point TilePositionPoint = new Point((int)Center.X / 32, (int)Center.Y / 32);
            Point TilePositionPointBelow = new Point((int)Center.X / 32, (int)Center.Y / 32 + (int)-Vector3.Down.Y);
            bool isOnLadder = false;
            bool isFalling = false;
            bool isRunnin = false;
            bool movedOnLadder = false;

            if (gameboard[TilePositionPoint] != null && gameboard[TilePositionPoint].IsLadder)
            {
                isOnLadder = true;
            }
            else if (gameboard[TilePositionPointBelow] != null && gameboard[TilePositionPointBelow].IsLadder)
            {
                if (Bottom > TilePositionPointBelow.Y * 32)
                {
                    isOnLadder = true;
                }
                else if (TilePositionPointBelow.Y * 32 > Bottom)
                {
                    float distance = TilePositionPointBelow.Y * 32 - Bottom;
                    isFalling = true;

                    if (distance > 3f)
                    {
                        amountToFall = 3f;
                    }
                    else
                    {
                        amountToFall = distance;
                    }
                }
            }
            else
            {
                if (gameboard[TilePositionPointBelow] == null || !gameboard[TilePositionPointBelow].IsSolid)
                {
                    amountToFall = 3f;
                    isFalling = true;
                }
                else if (TilePositionPointBelow.Y * 32 > Bottom)
                {
                    float distance = TilePositionPointBelow.Y * 32 - Bottom;
                    isFalling = true;

                    if (distance > 3f)
                    {
                        amountToFall = 3f;
                    }
                    else
                    {
                        amountToFall = distance;
                    }
                }
            }

            if (isFalling)
            {
                float distance = TilePositionPointBelow.Y * 32 - Bottom;

                if (distance > 3f)
                {
                    Y = centerTile.Y * 32;
                    amountToFall = distance;
                    gameboard.Camera.Move(Vector3.Down * amountToFall);
                }
                else
                {
                    Y += 3f;
                    gameboard.Camera.Move(Vector3.Down * amountToFall);
                    amountToFall -= 3f;
                }

                CheckIfShouldAdjustPosition(Vector3.Down);
            }
            else if (keyboardState.IsKeyDown(Keys.D))
            {
                MoveOrDig(new Vector2(Right, Center.Y), Vector3.Right, 2f);
                U = 0;
                isRunnin = true;
            }
            else if (keyboardState.IsKeyDown(Keys.A))
            {
                MoveOrDig(new Vector2(Left, Center.Y), Vector3.Left, 2f);
                U = 1;
                isRunnin = true;
            }
            else if (keyboardState.IsKeyDown(Keys.W))
            {
                if (isOnLadder)
                {
                    MoveOrDig(new Vector2(Center.X, Top), Vector3.Up, 2f);
                    movedOnLadder = true;
                }
                else
                {
                    MoveOrDig(new Vector2(Center.X, Center.Y - 32), Vector3.Up, 0f);
                }
            }
            else if (keyboardState.IsKeyDown(Keys.S))
            {
                MoveOrDig(new Vector2(Center.X, Bottom), Vector3.Down, 2f);

                if (isOnLadder)
                {
                    movedOnLadder = true;
                }
            }
            else
            {
                if (keyboardState.IsKeyDown(Keys.Z) && previousKeyboardState.IsKeyDown(Keys.Z))
                {
                    Tile tile = gameboard[centerTile.X, centerTile.Y];

                    if (tile != null && !tile.IsLadder)
                    {
                        tile.IsLadder = true;
                    }
                }

                diggingTimer = 0;
            }

            if (isRunnin)
            { 
                if (animationTimer == 10)
                {
                    if (V == 4)
                    {
                        V = 5;
                    }
                    else
                    {
                        V = 4;
                    }

                    animationTimer = 0;
                }
                else
                {
                    animationTimer++;
                }
            }
            else if (isOnLadder)
            {
                if (animationTimer == 0)
                {
                    U = 2;
                    V = 4;
                }
                else if (animationTimer == 15)
                {
                    if (V == 4)
                    {
                        U = 2;
                        V = 3;
                        animationTimer = 0;
                    }
                    else
                    {
                        U = 2;
                        V = 4;
                        animationTimer = 0;
                    }
                }

                if (movedOnLadder)
                {
                    animationTimer++;
                }
            }
            else
            {
                if (U == 2)
                {
                    U = 0;
                }
                V = 3;
            }

            for (int i = gameboard.Entities.Count - 1; i >= 0; i--)
            {
                Entity entity = gameboard.Entities[i];

                if (entity.Id == Id)
                {
                    continue;
                }

                if (entity.Contains(BoundingBox))
                {
                    if (entity is Chest chest)
                    {
                        chest.ProcessReward();
                        gameboard.Entities.Remove(chest);
                        Sfx.PlaySound("chest", 0.3f);
                    }
                }
            }

            previousKeyboardState = keyboardState;
        }

        private void MoveOrDig(Vector2 sidesToCheckWith, Vector3 direction, float speed)
        {
            Point next = new Point((int)(sidesToCheckWith.X + direction.X * speed) / 32, (int)(sidesToCheckWith.Y + -direction.Y * speed) / 32);
            Tile tile = gameboard[next.X, next.Y];

            if (tile != null && tile.IsSolid)
            {
                if (tile.CanBeDug)
                {
                    if (diggingTimer == 20)
                    {
                        gameboard.Score += gameboard[next.X, next.Y].Score;
                        gameboard[next.X, next.Y] = new DugOutTile(tile.NumberOfNearbyBombs);
                        diggingTimer = 0;

                        if (tile.IsBomb)
                        {
                            gameboard.CalculateBombNumbers();
                            gameboard.Lives--;
                            Sfx.PlaySound("bomb");
                        }

                        for (int nx = -1; nx <= 1; nx++)
                        {
                            for (int ny = -1; ny <= 1; ny++)
                            {
                                if (nx == 0 && ny == 0)
                                {
                                    continue;
                                }

                                Tile nearbyTile = gameboard[next.X + nx, next.Y + ny];

                                if (nearbyTile != null)
                                {
                                    nearbyTile.IsExplored = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (diggingTimer == 10)
                        {
                            Sfx.PlaySound("pickaxe", 0.25f);
                        }

                        if (direction.X != 0)
                        {
                            X = CenterTilePosition.X * 32;
                        }
                        else if (direction.Y != 0)
                        {
                            float floorY = CenterTilePosition.Y * 32 - Y;

                            if (floorY > 2f)
                            {

                            }
                            else
                            {
                                if (floorY != 0)
                                {
                                    gameboard.Camera.Move(new Vector3(0, -floorY, 0));
                                }

                                Y = CenterTilePosition.Y * 32;
                            }
                        }

                        diggingTimer++;
                    }
                }
                else
                {
                    X = CenterTilePosition.X * 32;
                    Y = CenterTilePosition.Y * 32;
                }
            }
            else
            {
                gameboard.Camera.Move(new Vector3(0, direction.Y * speed, 0));
                X += direction.X * speed;
                Y += -direction.Y * speed;
                diggingTimer = 0;
                CheckIfShouldAdjustPosition(direction);
            }

            lastDirection = direction;
        }

        private void CheckIfShouldAdjustPosition(Vector3 direction)
        {
            float xToAdjust = 0;
            float yToAdjust = 0;

            int tileBottom = (int)Bottom / 32 * 32;
            int tileTop = (int)Top / 32 * 32;
            int tileLeft = (int)Left / 32 * 32;
            int tileRight = (int)Right / 32 * 32;

            if (direction.X > 0) // Right
            {
                if ((int)Center.Y / 32 < (int)Bottom / 32)
                {
                    if (gameboard[(int)Right / 32, tileBottom / 32] != null && gameboard[(int)Right / 32, tileBottom / 32].IsSolid)
                    {
                        yToAdjust = Bottom - tileBottom;
                    }
                }
                else
                {
                    if (gameboard[(int)Right / 32, tileTop / 32] != null && gameboard[(int)Right / 32, tileTop / 32].IsSolid)
                    {
                        yToAdjust = -(Top - tileTop);
                    }
                }
            }
            else if (direction.X < 0) // Left
            {
                if ((int)Center.Y / 32 < (int)Bottom / 32)
                {
                    if (gameboard[(int)Left / 32, tileBottom / 32] != null && gameboard[(int)Left / 32, tileBottom / 32].IsSolid)
                    {
                        yToAdjust = Bottom - tileBottom;
                    }
                }
                else
                {
                    if (gameboard[(int)Left / 32, tileTop / 32] != null && gameboard[(int)Left / 32, tileTop / 32].IsSolid)
                    {
                        yToAdjust = -(Top - tileTop);
                    }
                }
            }
            else if (-direction.Y > 0) // Down
            {
                if ((int)Center.X / 32 > (int)Left / 32)
                {
                    if (gameboard[tileLeft / 32, (int)Bottom / 32] != null && gameboard[tileLeft / 32, (int)Bottom / 32].IsSolid)
                    {
                        xToAdjust = -(Left - tileLeft);
                    }
                }
                else
                {
                    if (gameboard[tileRight / 32, (int)Bottom / 32] != null && gameboard[tileRight / 32, (int)Bottom / 32].IsSolid)
                    {
                        xToAdjust = Right - tileRight;
                    }
                }
            }
            else if (-direction.Y < 0) // Up
            {
                if ((int)Center.X / 32 > (int)Left / 32)
                {
                    if (gameboard[tileLeft / 32, (int)Top / 32] != null && gameboard[tileLeft / 32, (int)Top / 32].IsSolid)
                    {
                        xToAdjust = -(Left - tileLeft);
                    }
                }
                else
                {
                    if (gameboard[tileRight / 32, (int)Top / 32] != null && gameboard[tileRight / 32, (int)Top / 32].IsSolid)
                    {
                        xToAdjust = Right - tileRight;
                    }
                }
            }

            if (xToAdjust > 2f)
            {
                X += -2f;
            }
            else if (xToAdjust < -2f)
            {
                X += 2f;
            }
            else
            {
                X += -xToAdjust;
            }

            if (yToAdjust > 2f)
            {
                Y += -2f;
                gameboard.Camera.Move(new Vector3(0, 2f, 0));
            }
            else if (yToAdjust < -2f)
            {
                Y += 2f;
                gameboard.Camera.Move(new Vector3(0, -2f, 0));
            }
            else
            {
                Y += -yToAdjust;
                gameboard.Camera.Move(new Vector3(0, yToAdjust, 0));
            }
        }
    }
}

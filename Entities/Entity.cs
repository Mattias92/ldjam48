﻿using LDJam48.World;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam48.Entities
{
    public abstract class Entity
    {
        protected Gameboard gameboard;
        public int Id { get; }

        public float X { get; set; }
        public float Y { get; set; }
        public Vector2 Position
        {
            get
            {
                return new Vector2(X, Y);
            }
            set
            {
                X = value.X;
                Y = value.Y;
            }
        }

        protected string texturePath;
        protected Texture2D texture;
        public static int Width = 32;
        public static int Height = 32;
        public int U { get; set; }
        public int V { get; set; }

        public Point CenterTilePosition
        {
            get
            {
                int x = (int)(X + Width / 2) / Width;
                int y = (int)(Y + Height / 2) / Height;
                return new Point(x, y);
            }
        }

        public Vector2 Center
        {
            get
            {
                return new Vector2(X + Width / 2, Y + Height / 2);
            }
        }

        public float Left
        {
            get
            {
                return X;
            }
        }

        public float Right
        {
            get
            {
                return X + 32f;
            }
        }

        public float Top
        {
            get
            {
                return Y;
            }
        }

        public float Bottom
        {
            get
            {
                return Y + 32f;
            }
        }

        public Entity(int id, Gameboard gameboard)
        {
            Id = id;
            this.gameboard = gameboard;
            U = 0;
            V = 0;
        }

        public BoundingBox BoundingBox
        {
            get
            {
                return new BoundingBox(new Vector3(Left + 1, Top + 1, 0), new Vector3(Right - 1, Bottom - 1, 0));
            }
        }

        public bool Contains(BoundingBox boundingBox)
        {
            return boundingBox.Intersects(BoundingBox);
        }

        public virtual void Update(float delta)
        {

        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, Position, new Rectangle(U * Width, V * Height, Width, Height), Color.White);
        }
    }
}

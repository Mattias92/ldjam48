﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDJam48
{
    public class Camera
    {
        public Vector3 Position { get; set; }

        private float width;
        private float height;
        private float zNearPlane;
        private float zFarPlane;

        public Camera(float width, float height, float zNearPlane, float zFarPlane)
        {
            this.width = width;
            this.height = height;
            this.zNearPlane = zNearPlane;
            this.zFarPlane = zFarPlane;

            Position = Vector3.Zero;
        }

        public Matrix ViewMatrix
        {
            get
            {
                return Matrix.CreateTranslation(Position);
            }
        }

        public void Move(Vector3 change)
        {
            Position += change;
        }
    }
}

﻿using LDJam48.Entities;
using LDJam48.World;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace LDJam48
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;
        private Gameboard gameboard;
        private SpriteFont testFont;
        private Camera camera;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
            
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            Window.Title = "Goldsweeper";
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            camera = new Camera(GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height, 1, 0);
            gameboard = new Gameboard(Content, camera, GraphicsDevice.Viewport.Width);
            testFont = Content.Load<SpriteFont>("LatoRegular12");

            Sfx.SoundEffects.Add("bomb", Content.Load<SoundEffect>("sounds/bomb"));
            Sfx.SoundEffects.Add("pickaxe", Content.Load<SoundEffect>("sounds/pickaxe3"));
            Sfx.SoundEffects.Add("chest", Content.Load<SoundEffect>("sounds/win2"));

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                Exit();
            }

            if (gameboard.Lives > 0)
            {
                gameboard.Update(0);
            }
            else
            {
                // Deleting all entities to prevent odd camera glitch (entites get stuck in memory perhaps?)
                for (int i = 0; i < gameboard.Entities.Count; i++) 
                {
                    gameboard.Entities[i] = null;
                }

                if (Keyboard.GetState().IsKeyDown(Keys.Enter))
                {
                    camera = new Camera(GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height, 1, 0);
                    gameboard = new Gameboard(Content, camera, GraphicsDevice.Viewport.Width);
                }
            }
                
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            if (gameboard.Lives > 0)
            {
                GraphicsDevice.Clear(Color.CornflowerBlue);
                spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, null, camera.ViewMatrix);
                gameboard.Draw(spriteBatch);
                spriteBatch.End();

                spriteBatch.Begin();
                gameboard.DrawLives(spriteBatch);
                spriteBatch.End();
            }
            else
            {
                GraphicsDevice.Clear(Color.Black);
                spriteBatch.Begin();
                float widthSectionPerLine = GraphicsDevice.Viewport.Height / 3;
                string gameoverStr = "Game Over";
                string finalScoreStr = "Score: $" + gameboard.Score;
                string continueStr = "Press Enter to play again!";
                Vector2 gameoverStrSize = testFont.MeasureString(gameoverStr);
                Vector2 finalScoreStrSize = testFont.MeasureString(finalScoreStr);
                Vector2 continueStrSize = testFont.MeasureString(continueStr);
                spriteBatch.DrawString(testFont, gameoverStr, new Vector2(GraphicsDevice.Viewport.Width / 2 - gameoverStrSize.X / 2, widthSectionPerLine / 2 - gameoverStrSize.Y / 2), Color.White);
                spriteBatch.DrawString(testFont, finalScoreStr, new Vector2(GraphicsDevice.Viewport.Width / 2 - finalScoreStrSize.X / 2, widthSectionPerLine / 2 * 2 - finalScoreStrSize.Y / 2), Color.White);
                spriteBatch.DrawString(testFont, continueStr, new Vector2(GraphicsDevice.Viewport.Width / 2 - continueStrSize.X / 2, widthSectionPerLine / 2 * 3 - continueStrSize.Y / 2), Color.White);
                spriteBatch.End();
            }

            base.Draw(gameTime);
        }
    }
}
